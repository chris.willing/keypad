# Keypad.py

I wanted to try some apps I'd written for ESP32 based boards on a Rasbperri Pi Zero W. The apps use a cheap, commonly available, matrix keypad for occasional user input and there exists a good Keypad library for programing the ESP32 using the Arduino IDE.

!["Matrix keypad"](images/keypad.png)

When searching for a Python based keypad library to use with Raspberry Pi based boards, nothing I could find was able to capture the UP component of a button press; only the DOWN component. Additionally, the found codes mostly used polling of the keypad in an endless loop - fine for a demo but not so efficient in a real application.

A Javascript version `keypad.js` is also now available.

Both of the code snippets provided here use interrupts to detect and report both UP and DOWN keypresses. An infinite sleep loop is used in the Python snippet only to keep the code running and would be removed when incorporating the snippet into an app with its own event loop.

Examples `keypad-mqtt.py` and `keypad-mqtt.js` showing how to incorporate the keypad snippets into a real applications are available in the [examples](examples/) directory.


# Setup

For the Python version, the RPi.GPIO python module is required. Install it with:
```
    sudo apt install python3-rpi.gpio
```

For the Javascript version, GPIO is performed by the `onoff` module. Install it with:
```
    npm install onoff
```

Any available pins may be used to attach the keypad to the pi. The actual pins used should be entered into the code's ROW_PINS and COL_PINS arrays. The built in values are for a 4x4 keypad. A 4x3 keypad would have only three pin numbers in the COL_PINS array. The code is designed to tolerate keypad matrices of any dimension although, obviously, the dimensions will ultimately be restricted by the number of available GPIO pins on the pi.



# Operation

When running, both code snippets will print a number (0-15 for a 4x4 keypad, 0-12 for 4x3 etc.) and the word DOWN or UP when a key is pressed or released.

When included in a real app., change the lines `print("{} DOWN".format(control_id))` and `print("{} UP".format(button_down))` to whatever is appropriate for the app. i.e. perform whatever action you want to be associated with the key event.

For the Javascript version, change the lines `console.log(control_id + "DOWN");` and `console.log(button_down + "UP");`


# Support

Problems, comments and questions should be directed to the [Issues](https://gitlab.com/chris.willing/keypad/-/issues) section.


# License

MIT


# Links

- [https://maker.pro/raspberry-pi/tutorial/how-to-use-a-keypad-with-a-raspberry-pi-4](https://maker.pro/raspberry-pi/tutorial/how-to-use-a-keypad-with-a-raspberry-pi-4)
- [https://www.donskytech.com/setup-keypad-with-raspberry-pi/](https://www.donskytech.com/setup-keypad-with-raspberry-pi/)
- [https://github.com/brettmclean/pad4pi](https://github.com/brettmclean/pad4pi)
- [https://blog.geggus.net/2017/08/a-matrix-keypad-on-a-raspberry-pi-done-right/](https://blog.geggus.net/2017/08/a-matrix-keypad-on-a-raspberry-pi-done-right/)
