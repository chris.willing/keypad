#!/usr/bin/env python3

import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

ROW_PINS = [5, 6, 13, 19]
COL_PINS = [12, 16, 20, 21]

button_down = -1

def event_callback(pin):
    global button_down
    if button_down > -1:
        print("{} UP".format(button_down))
        button_down = -1
        return

    # We don't use this
    #value = GPIO.input(pin)

    active_column = COL_PINS.index(pin)
    for row in range(len(ROW_PINS)):
        GPIO.output(ROW_PINS[row], GPIO.HIGH)
        if (GPIO.input(COL_PINS[active_column]) == GPIO.HIGH):
            control_id = len(ROW_PINS)*active_column + row
            print("{} DOWN".format(control_id))
            button_down = control_id
            GPIO.output(ROW_PINS[row], GPIO.LOW)
            return
        GPIO.output(ROW_PINS[row], GPIO.LOW)


for row_pin in ROW_PINS:
    #print("row_pin: {}".format(row_pin))
    GPIO.setup(row_pin, GPIO.OUT)
    GPIO.output(row_pin, GPIO.LOW)

for col_pin in COL_PINS:
    #print("col_pin: {}".format(col_pin))
    GPIO.setup(col_pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
    GPIO.add_event_detect(col_pin, GPIO.BOTH,
		  callback=event_callback,
		  bouncetime=120)

try:
    while True:
        time.sleep(100)
except KeyboardInterrupt:
    GPIO.cleanup()

