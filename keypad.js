#!/usr/bin/env node

var Gpio = require('onoff').Gpio;

ROW_PINS = [5, 6, 13, 19];
COL_PINS = [12, 16, 20, 21];

row_pinobjs = [];
col_pinobjs = [];

button_down = -1;

for (pin of ROW_PINS) {
	rowobj = {};
	rowobj['pin_id'] = pin;
	rowobj['pushbutton'] = new Gpio(pin, 'out');
	rowobj['pushbutton'].writeSync(Gpio.LOW);
	row_pinobjs.push(rowobj);

}

for (pin of COL_PINS) {
	pinobj = {};
	pinobj['pin_id'] = pin;
	pinobj['pushbutton'] = new Gpio(pin, 'in', 'both');
	pinobj['pushbutton'].watch(function (err, value) {
		if (err) {
			console.error('There was an error', err);
			return;
		}
		if ((button_down > -1) || (value == 1)) {
			if ((button_down > -1) && (value == 1)) {
				// Do something with this button (button_down)
				console.log(button_down + " UP");

				button_down = -1;
			}
			return;
		}
		active_column = COL_PINS.indexOf(this.pin_id);
		if (active_column < 0) return;
		for (row in ROW_PINS) {
			rowobj = row_pinobjs[row];
			rowobj['pushbutton'].writeSync(Gpio.HIGH);
			if (col_pinobjs[active_column]['pushbutton'].readSync() == Gpio.HIGH) {
				control_id = parseInt(active_column)*parseInt(row_pinobjs.length) + parseInt(row);

				// Do something with this button (control_id)
				console.log(control_id + " DOWN");
				button_down = control_id
			}
			rowobj['pushbutton'].writeSync(Gpio.LOW);
		}
	}.bind(pinobj));	
	col_pinobjs.push(pinobj);
}


function unexportOnClose() {
	for (obj of col_pinobjs) {
		obj.pushbutton.unexport();
	}
}
