# MQTT Example

The `keypad-mqtt.py` and `keypad-mqtt.js` files here are examples of how to incorporate the keypad code snippet into a real, albeit minimal, application. Especially note that the original Python snippet's infinite sleep loop is replaced by MQTT's `client.loop_forever()`.

In addition to the previously installed `RPi.GPIO` module, the Python example requires the `paho-mqtt` module to be available. Install it with:
```
    sudo apt install python3-paho-mqtt
```
THe Javascript example requires the `mqtt` module. Install it with:
```
    npm install mqtt
```

When run, each example will:
- connect to a public MQTT broker
- define an arbitrary topic name and subscribe to it (we use `PUBSUB_TOPIC = "test/keypad"`)
- at each DOWN and UP event, print a message locally and also publish it to the PUBSUB_TOPIC

Since we have subscribed to the same topic that we publish to, we will see an echo from the broker of whatever we send it. Therefore a typical session will look something like:
```
    alice@raspberrypi:~/src/keypad $ ./examples/keypad-mqtt.py 
    Connected successfully with result code 0
    Subscription looks OK - message id = 1
     Sending message: {"control_id": 0, "updown": "down"}
    Received message: {"control_id": 0, "updown": "down"}
     Sending message: {"control_id": 0, "updown": "up"}
    Received message: {"control_id": 0, "updown": "up"}
     Sending message: {"control_id": 12, "updown": "down"}
    Received message: {"control_id": 12, "updown": "down"}
     Sending message: {"control_id": 12, "updown": "up"}
    Received message: {"control_id": 12, "updown": "up"}
     Sending message: {"control_id": 7, "updown": "down"}
    Received message: {"control_id": 7, "updown": "down"}
     Sending message: {"control_id": 7, "updown": "up"}
    Received message: {"control_id": 7, "updown": "up"}
     Sending message: {"control_id": 3, "updown": "down"}
    Received message: {"control_id": 3, "updown": "down"}
     Sending message: {"control_id": 3, "updown": "up"}
    Received message: {"control_id": 3, "updown": "up"}
```

We see that for each key DOWN event, a local print ("Sending message:") of the JSON formatted message is followed by reception of that message from the broker ("Received message:"). Similarly for key UP events. If key UP events are of no interest, just comment out local printing and publishing of the UP event.
