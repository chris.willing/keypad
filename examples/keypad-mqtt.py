#!/usr/bin/env python3

import time
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
import json

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

ROW_PINS = [5, 6, 13, 19]
COL_PINS = [12, 16, 20, 21]

MQTT_URL = "broker.emqx.io"
#MQTT_URL = "test.mosquitto.org"
PUBSUB_TOPIC = "test/keypad"

button_down = -1

def event_callback(pin):
    global button_down
    if button_down > -1:
        #print("{} UP".format(button_down))
        msg = json.dumps({"control_id":button_down, "updown":"up"})
        print(f" Sending message: {msg}")
        client.publish(PUBSUB_TOPIC, msg)
        button_down = -1
        return

    # We don't use this
    #value = GPIO.input(pin)

    active_column = COL_PINS.index(pin)
    for row in range(len(ROW_PINS)):
        GPIO.output(ROW_PINS[row], GPIO.HIGH)
        if (GPIO.input(COL_PINS[active_column]) == GPIO.HIGH):
            control_id = len(ROW_PINS)*active_column + row
            #print("{} DOWN".format(control_id))
            msg = json.dumps({"control_id":control_id, "updown":"down"})
            print(f" Sending message: {msg}")
            client.publish(PUBSUB_TOPIC, msg)
            button_down = control_id
            GPIO.output(ROW_PINS[row], GPIO.LOW)
            return
        GPIO.output(ROW_PINS[row], GPIO.LOW)


for row_pin in ROW_PINS:
    #print("row_pin: {}".format(row_pin))
    GPIO.setup(row_pin, GPIO.OUT)
    GPIO.output(row_pin, GPIO.LOW)

for col_pin in COL_PINS:
    #print("col_pin: {}".format(col_pin))
    GPIO.setup(col_pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
    GPIO.add_event_detect(col_pin, GPIO.BOTH,
		  callback=event_callback,
		  bouncetime=120)


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print(f"Connected successfully with result code {rc}")
        client.subscribe(PUBSUB_TOPIC)
    else:
        print(f"Connected fail with code {rc}")
        sys.exit(1)

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscription looks OK - message id = {}".format(mid))

def on_publish(client, userdata, mid):
    #print("Message published with message id: {}".format(mid))
    pass

def on_message(client, userdata, message):
    print("Received message: {}".format(str(message.payload.decode("utf-8"))))



# Establish client reference
client = mqtt.Client()

# Setup MQTT callbacks
client.on_connect = on_connect
client.on_subscribe = on_subscribe
client.on_publish = on_publish
client.on_message = on_message

# Connect to broker
#client.connect(MQTT_URL, 1883, 60) 
client.connect(MQTT_URL, 1883, 60) 

# Stay alive
client.loop_forever()

