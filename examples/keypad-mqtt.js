#!/usr/bin/env node

const os = require('os');
const Gpio = require('onoff').Gpio;
const mqtt = require('mqtt');

const ROW_PINS = [5, 6, 13, 19];
const COL_PINS = [12, 16, 20, 21];

const MQTT_URL = "mqtt://broker.emqx.io";
//const MQTT_URL = "mqtt://test.mosquitto.org";
const PUBSUB_TOPIC = "test/keypad";

const row_pinobjs = [];
const col_pinobjs = [];

var button_down = -1;

const client = mqtt.connect(MQTT_URL);

client.on('connect', (err) => {
	console.log(`connected to server ${MQTT_URL}`);	
	client.subscribe(PUBSUB_TOPIC, function (err) {
		if (!err) {
			console.log(`subscribed to MQTT server OK`);
		} else {
			// Any point in going on?
			console.log('Exiting due to MQTT subscription failure: ' + err);
			process.exit(1);
		}
	});
});
client.on('message', (topic, message) => {
	console.log(`Received message from topic: ${topic}, message: ${message}`);	
});


for (pin of ROW_PINS) {
	rowobj = {};
	rowobj['pin_id'] = pin;
	rowobj['pushbutton'] = new Gpio(pin, 'out');
	rowobj['pushbutton'].writeSync(Gpio.LOW);
	row_pinobjs.push(rowobj);

}

for (pin of COL_PINS) {
	pinobj = {};
	pinobj['pin_id'] = pin;
	pinobj['pushbutton'] = new Gpio(pin, 'in', 'both');
	pinobj['pushbutton'].watch(function (err, value) {
		if (err) {
			console.error('There was an error', err);
			return;
		}
		//console.log(`Pin ${this.pin_id} ${value==0?"DOWN":"UP"} (${value})`);
		if ((button_down > -1) || (value == 1)) {
			if ((button_down > -1) && (value == 1)) {
				// Do something with this button (button_down)
				//console.log(`${button_down} UP`);
				var msg = JSON.stringify({"control_id":button_down, "updown":"up"});
				console.log(` Sending message: ${msg}`);
				client.publish(PUBSUB_TOPIC, msg)

				button_down = -1;
			}
			return;
		}
		active_column = COL_PINS.indexOf(this.pin_id);
		if (active_column < 0) return;
		for (row in ROW_PINS) {
			rowobj = row_pinobjs[row];
			rowobj['pushbutton'].writeSync(Gpio.HIGH);
			//if (GPIO.input(COL_PINS[active_column]) == GPIO.HIGH):
			if (col_pinobjs[active_column]['pushbutton'].readSync() == Gpio.HIGH) {
				control_id = parseInt(active_column)*parseInt(row_pinobjs.length) + parseInt(row);

				// Do something with this button (control_id)
				//console.log(`${control_id} DOWN`);
				button_down = control_id
				var msg = JSON.stringify({"control_id":button_down, "updown":"down"});
				console.log(` Sending message: ${msg}`);
				client.publish(PUBSUB_TOPIC, msg)
			}
			rowobj['pushbutton'].writeSync(Gpio.LOW);
		}
	}.bind(pinobj));	
	col_pinobjs.push(pinobj);
}


function unexportOnClose() {
	for (obj of col_pinobjs) {
		obj.pushbutton.unexport();
	}
}

